		<!--ALERTA PORTAL-->
		<style>
			
			#maska{
				display:block;
				position: fixed;
				top:0;
				left:0;
				width:100%;
				height:100%;
				background:rgba(0,0,0,.4);
				z-index:50;
				opacity:0;
				}
			
		
             #cuadroAlert{
            	margin:0 auto;
             	margin-top:10%;
				width: 874px;
				height: 325px;
				padding:30px 0 0 0;
				background:white;
				border-radius:5px;
				position:relative;
				-webkit-box-shadow: 3px 3px 15px rgba(50, 50, 50, 0.75);
				-moz-box-shadow:    3px 3px 15px rgba(50, 50, 50, 0.75);
				box-shadow:         3px 3px 15px rgba(50, 50, 50, 0.75);
				}
				
			#cuadroAlert img{
					margin:0 auto;
					width:auto;
					margin-top: -8px;
					}	
				
			.cerrar{
			    padding:5px;:
				width:auto;
				height:12px;
				line-height:12px;
				color:white;
				font-weight:bold;
				border-radius:4px;
				background:#3399FF;
				cursor:pointer;
				position:absolute;
				right:5px;
				top:10px;
				}		
		
		</style>
		
		<script>
		
			$(document).ready(function(){
			
			$('#maska').stop().animate({
				'opacity':1
			});


			$('.cerrar').click(function(){
				$('#maska').stop().animate({
					'opacity':0
				},function(){
					$('#maska').css('display','none');
				});
			});
					
			})			
		
		</script>
		
		<div id="maska">
			<div id="cuadroAlert">
				<div class="cerrar">Cerrar</div>
				<img alt="Alerta" src="http://www.cibercolegioucn.edu.co/PublishingImages/navidad-01.png" width="100%"/><a href="http://www.cibercolegioucn.edu.co/Paginas/Proceso_renovacion.aspx"><img alt="Alerta" src="/PublishingImages/renovacion_matricula-01.png" width="100%"/></a>
			</div>
		</div>
						
		<!--FIN ALERTA PORTAL-->