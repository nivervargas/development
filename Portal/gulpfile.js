'use strict';

// GULP
var gulp = require('gulp');

// DEPENDENCIES
var $ = require('gulp-load-plugins')();
var concat  = require('gulp-concat'),
    minifyCSS = require('gulp-minify-css')

var env = process.env.NODE_ENV || 'development';
var outputDir = 'compile';

// Automatically Prefix CSS
gulp.task('styles:css', function () {
	var a = [
		'css/style.css',
		'css/layer-slider.css',
		'css/*.css',
		'css/**/*.css'
	];
    return gulp.src(a)
        .pipe(concat('styles.css'))
        .pipe(minifyCSS({keepBreaks:true}))
        .pipe(gulp.dest('compile/styles/'))
        .pipe($.size({title: 'styles:css'}))
});

// Concat application javascript
gulp.task('appjs', function () {
	var a = [
		'jss/jquery.min.js',
		'jss/modernizr.custom.js',
		'jss/layerslider.transitions.js',
		'jss/jquery.layerslider.js',
		'jss/*.js'
	]
	gulp.src(a)
		.pipe($.plumber())
		// .pipe(uglif'app/scripts/app/lessons/**/*.js'y())
		.pipe(concat('app.js'))
		.pipe(gulp.dest('compile/scripts'))
		.pipe($.size({title: 'script:app'}))
});



gulp.task('watch', function () {
	gulp.watch('css/*.css', ['styles:css']);
	gulp.watch('jss/*.jss', ['appjs']);
	
});

gulp.task('default', ['styles:css', 'appjs', 'watch']);

// gulp.task('default', ['jshint']);